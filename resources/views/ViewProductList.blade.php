@extends('layouts.app')

@section('titlePage')Магазин@endsection

@section('content')
      <div class="single-products-catagory clearfix">
        <h1>Список продуктов</h1>
      </div>

      <!-- Single Catagory -->
      <div class="single-products-catagory clearfix">
        <a href="shop.html">
          <img src="img/bg-img/1.jpg" alt="">
          <!-- Hover Content -->
          <div class="hover-content">
            <div class="line"></div>
            <p>From $180</p>
            <h4>Modern Chair</h4>
          </div>
        </a>
      </div>

      <!-- Single Catagory -->
      <div class="single-products-catagory clearfix">
        <a href="shop.html">
          <img src="img/bg-img/2.jpg" alt="">
          <!-- Hover Content -->
          <div class="hover-content">
            <div class="line"></div>
            <p>From $180</p>
            <h4>Minimalistic Plant Pot</h4>
          </div>
        </a>
      </div>

      <!-- Single Catagory -->
      <div class="single-products-catagory clearfix">
        <a href="shop.html">
          <img src="img/bg-img/3.jpg" alt="">
          <!-- Hover Content -->
          <div class="hover-content">
            <div class="line"></div>
            <p>From $180</p>
            <h4>Modern Chair</h4>
          </div>
        </a>
      </div>
    </div>
  </div>
@endsection