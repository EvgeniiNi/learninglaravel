<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\ModelProduct;
use App\User;
use Illuminate\Http\Request;

class ControllerAdminProduct extends Controller
{
  private $path = __DIR__ . '/../../../public/files/';
  private $folderImageProduct = '/files/';

  public function actList()
  {
    $products = ModelProduct::all();
    return view('adm/ViewProductList', $products);
  }

  public function actCreate()
  {
    return view('adm.ViewProductCreate');
  }

  public function actView($id)
  {
    $db = ModelProduct::find($id)->get();
    return view('adm/ViewProductEdit', ['data' =>$db]);
  }

  public function actSave(Request $request)
  {
    $db = new ModelProduct();
    if (!file_exists($this->path . $request['name_product'])) {
      $db->name_product = $request['name_product'];
      $db->review_product = $request['review_product'];
      $db->price_product = (float)$request['price_product'];
      $db->addition_information_product = $request['addition_information_product'];
      $db->image_product = '';
      $db->save();
    } else {
      $db->where('name_product', '=', $request['name_product'])->update(
        [
          'name_product' => $request['name_product'],
          'review_product' => $request['review_product'],
          'price_product' => (float)$request['price_product'],
          'addition_information_product' => $request['addition_information_product']
        ]
      );
    }
    return redirect()->route('admin')->with('success', 'Данные успешно сохранены');
  }

  public function actDelete($id)
  {
    return redirect()->route('admin')->with('success', 'Данные успешно удалены');
  }

  public function actSaveFile(Request $request)
  {
    if (!file_exists($this->path . $request['name'])) {
      mkdir($this->path . $request['name'], 0777);
    }
    move_uploaded_file($_FILES['image']['tmp_name'], $this->path . $request['name'] . '/'
      . basename($_FILES['image']['name']));
    $db = new ModelProduct();
    $db->name_product = $request['name'];
    $db->review_product = '';
    $db->price_product = 0;
    $db->addition_information_product = '';
    $db->image_product = $this->folderImageProduct . $request['name'] . '/';
    $db->save();
  }
}
