<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ControllerAdminHome extends Controller
{
  public function actList()
  {
    return view('adm/ViewAdmHome', ['data'=>['singIn' => $this->isSignIn(), 'access' => $this->isAdmin()]]);
  }
}
