<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControllerProduct extends Controller
{
    public function actList()
    {

      return view('ViewProductList');
    }

    public function actView($id)
    {
      return view('ViewProductView');
    }
}
