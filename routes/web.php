<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ControllerHome@actView')->name('home');
Route::get('/products', 'ControllerProduct@actList')->name('products');
Route::get('/product/{product}', 'ControllerProduct@actView')->name('show');
Route::get('/review', 'ControllerReview@actView')->name('review');
Route::get('/contacts', 'ControllerContacts@actView')->name('contacts');
Route::get('/search', 'ControllerHome@actView')->name('search');

// Роуты для аутентифицированного пользователя
Route::get('/trash', 'ControllerHome@actView')->name('trash');
Route::get('/favorite', 'ControllerHome@actView')->name('favorite');

// Роуты админки
Route::get('/admin', 'ControllerAdminHome@actList')->name('admin');
Route::get('/admin/product/create', 'ControllerAdminProduct@actCreate')->name('create');
Route::post('/admin/product/create', 'ControllerAdminProduct@actSave')->name('save');
Route::post('/admin/product/create/file', 'ControllerAdminProduct@actSaveFile')->name('save-file');
Route::get('/admin/product/{product}/edit', 'ControllerAdminProduct@actView')->name('edit');
Route::post('/admin/product/{product}/edit', 'ControllerAdminProduct@actUpdate')->name('update');
Route::get('/admin/product/{product}/delete', 'ControllerAdminProduct@actDelete')->name('delete');

// Роуты аутентификации
Auth::routes();